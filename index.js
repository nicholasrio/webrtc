//init
var video = $("#webRTCtest")[0];
var canvas;
var photo = $("#photo")[0];
var button = $("#pictButton");

var width = 640;
var height = 320;

var constraints = {
	audio: false,
	video: {
		facingMode: "user"
	}
};
/*
NOTE BUAT FACINGMODE

	facingMode: "user" //by default kamera depan
	facingMode: "environment" //by default kamera belakang

	facingMode:{
		exact: "user" //harus kamera depan
	}

	facingMode:{
		exact: "environment" //harus kamera blkg
	}
*/

navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream){
	video.srcObject = mediaStream;
	video.onloadedmetadata = function(e) {
		video.play();
	}
}).catch(function(err){
	console.log(err.name + " : " + err.message);
});

function clearPhoto(){
	var context = canvas.getContext('2d');
	context.fillStyle = "#AAA";
	context.fillRect(0, 0, canvas.width, canvas.height);

	var data = canvas.toDataURL('image/png');
    photo.setAttribute('src', data);
}

function takePicture(){
	width = video.videoWidth;
	height = video.videoHeight;
	console.log("size : " + width + " x " + height);

	var tempcanvas = $("<canvas width=" + width + " height=" + height + " />");
	canvas = tempcanvas[0];

	console.log(canvas.height);
	console.log(canvas.width);

	var context = canvas.getContext('2d');
	context.drawImage(video, 0, 0, width, height);

	var data = canvas.toDataURL('image/png');
    photo.setAttribute('src', data);
}

button.click(function(){
	takePicture();
});